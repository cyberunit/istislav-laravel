<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item', function (Blueprint $table) {
            $table->bigIncrements('miid')->comment('ID');
            $table->integer('pid')->unsigned()->nullable(false)->default(0)->comment('Parent ID');
            $table->string('alias')->default('')->unique()->comment('URL Alias');
            $table->integer('mid')->unsigned()->nullable(false)->default(0)->comment('Menu ID');
            $table->integer('sub_mid')->unsigned()->nullable(false)->default(0)->comment('Submenu ID');
            $table->string('type')->default('')->comment('Link Type');
            $table->string('link')->default('')->comment('Link');
            $table->string('title')->default('')->comment('Title');
            $table->integer('weight')->nullable(false)->default(0)->comment('Weight');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item');
    }
}

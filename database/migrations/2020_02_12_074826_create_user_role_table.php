<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->integer('uid')->unsigned()->nullable(false)->default(0)->comment('User ID');
            $table->integer('rid')->unsigned()->nullable(false)->default(0)->comment('Role ID');
            $table->timestamps();
            $table->unique('uid', 'rid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
    }
}

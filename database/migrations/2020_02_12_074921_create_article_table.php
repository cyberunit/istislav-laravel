<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->bigIncrements('aid')->comment('ID');
            $table->string('title')->default('')->comment('Title');
            $table->integer('uid')->unsigned()->nullable(false)->default(0)->comment('User ID');
            $table->text('body')->nullable()->comment('Body');
            $table->boolean('status')->unsigned()->nullable(false)->default(0)->comment('Status');
            $table->timestamps();
            $table->index('uid');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}

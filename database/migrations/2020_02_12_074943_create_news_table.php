<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('nid')->comment('ID');
            $table->string('title')->default('')->comment('Title');
            $table->dateTime('date')->nullable()->comment('Date and time');
            $table->integer('uid')->unsigned()->nullable(false)->default(0)->comment('User ID');
            $table->integer('article_id')->unsigned()->nullable(false)->default(0)->comment('Article ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_Item extends Model
{
    protected $primaryKey = 'miid';

    public function menu() {
        //return $this->hasOne('App\Menu', 'mid', 'mid') - it is contrariwise operation of external table link
        // hasOne search in another table by current primary ID and another menu_item_id field. It is not our way!
        return $this->belongsTo('App\Menu', 'mid', 'mid');
    }

    public function submenu() {
        return $this->belongsTo('App\Menu', 'sub_mid', 'mid');
    }


}

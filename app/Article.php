<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primaryKey = 'aid';
    public function user() {
        return $this->belongsTo('App\User', 'uid', 'id');
    }
}
